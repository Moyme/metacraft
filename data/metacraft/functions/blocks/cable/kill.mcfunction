#delete connections
execute as @s[tag=South] positioned ~ ~-0.768 ~0.31 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=North] positioned ~ ~-0.768 ~-0.31 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=East] positioned ~0.31 ~-0.768 ~ run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=West] positioned ~-0.31 ~-0.768 ~ run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=Up] positioned ~ ~-0.66 ~-0.205 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
execute as @s[tag=Down] positioned ~ ~-1.283 ~-0.205 run kill @e[tag=CableConnection,distance=..0,limit=1,sort=nearest]
#delete cable
kill @s
#disconnect
execute as @e[tag=Cable,distance=..3] run data merge entity @s {Marker:0b}
execute as @e[tag=Cable,distance=..1] at @s run function metacraft:blocks/cable/disconnect
execute as @e[tag=Cable,distance=..3] run data merge entity @s {Marker:1b}