setblock ~ ~ ~ air
setblock ~ ~ ~ minecraft:player_head{Owner:{Id:"00000000-0000-0001-0000-000000000000",Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTYxYTc3ZDUzZjQ1ZTMxYjBmZjA0ZThjNGZmNzg3ZTFmN2YzMzc4Y2VjNjVjYThhODlmYWY1NThmMDQ5ZDI5NiJ9fX0="}]}}}
summon minecraft:armor_stand ~ ~ ~ {Tags:["Cable","ConnectSouth","ConnectNorth","ConnectEast","ConnectWest","ConnectUp","ConnectDown"],NoBasePlate:1b,Invulnerable:1b,Invisible:1b,NoGravity:1b,Small:1b,Marker:1b}
execute as @e[tag=Cable,sort=nearest,limit=1] at @s run scoreboard players set @s ObjectMaxEnergy 100
execute as @e[tag=Cable,sort=nearest,limit=1] at @s run scoreboard players set @s ObjectEnergyIO 10
#connect
execute as @e[tag=Cable,distance=..3] run data merge entity @s {Marker:0b}
execute as @e[tag=Cable,distance=..1] at @s run function metacraft:blocks/cable/connect
execute as @e[tag=Cable,distance=..3] run data merge entity @s {Marker:1b}