scoreboard objectives add ticks dummy
scoreboard objectives add ticksecond dummy
scoreboard objectives add one dummy
scoreboard players set @e one 1
scoreboard objectives add zero dummy
scoreboard players set @e zero 0
scoreboard objectives add twenty dummy
scoreboard players set @e twenty 20
scoreboard objectives add placePlayerHead minecraft.used:minecraft.player_head
#scoreboard objectives add minePlayerHead minecraft.mined:minecraft.player_head